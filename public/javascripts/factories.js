// Auth
app.factory('Auth', ['$state','toastr','$rootScope','$http','$cookies','$window',function($state,toastr,$rootScope,$http,$cookies, $window){
	var object = {
		user: false,
		gettingUser: false,
		getToken: function(cb){
			return $cookies.get('token');
		},
		forgotPassword: function(model,cb){
			$http.post('http://50.21.179.35:8082/users/forgotpassword',model)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					if(cb)
						cb(false);
					return toastr.error(response.data.mes,'',{progressBar:true});
				}
			},function(error){
				if(cb)
					cb(false);
				return toastr.error('Error de conexión','',{progressBar:true});
			});
		},
		resetPassword: function(object,cb) {
			$http.put('http://50.21.179.35:8082/users',object)
			     .then(function(response) {
					 if(response.data.e == true)
						return cb?cb(true):false;
					 else
						if(cb)
							cb(false)
					 	return toastr.error(response.data.mes,'',{progressBar:true});
				 },function(err) {
					 if(cb)
						cb(false);
					return toastr.error('Error de conexion','',{progressBar:true});
				 });
		},
		getTokenSecret: function(cb){
			return $cookies.get('tokenSecret');
		},
		getUser: function(cb){
			if(this.gettingUser)
				return;
			let token = this.getToken();
			let tokenSecret = this.getTokenSecret();
			this.gettingUser = true;
			$http.get('http://50.21.179.35:8082/users?token='+token+'&tokensecret='+tokenSecret)
			.then(function(response){
				this.gettingUser = false;
				if(response.data.e == 0){
					this.user = angular.copy(response.data.item);
					$rootScope.$emit('gotUser',true);
					if(cb)
						cb(true, response.data.item);
				}else{
					if(cb)
						cb(false);
					return toastr.error(response.data.mes,'', {progressBar:true});
				}
			}.bind(this),
			function(error){
				this.gettingUser = false;
				if(cb)
					cb(false);
				return toastr.error('Error de conexión','', {progressBar:true});
			});
		},
		isLogged: function(){
			let bool = $cookies.get('token') && $cookies.get('tokenSecret');
			if(bool && !this.user._id)
				this.getUser();
			return bool;
		},
		login: function(object,cb, back){
			if(!object.mail || !object.password){
				if(cb)
					cb(false);
				return toastr.error('Debes enviar el Usuario y la contraseña','', {progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/auth/login',object)
			.then(function(response){
				if(response.data.e == 0){
					$cookies.put('token',response.data.token);
					$cookies.put('tokenSecret',response.data.tokenSecret);
					this.getUser(cb);
					$rootScope.$emit('logged', true);
					// if(back)
					// 	$window.history.back();
					// else
					// 	$state.go('base.index');
				}else{
					if(cb)
						cb(false, response.data.e == 5);
					return toastr.error(response.data.mes,'', {progressBar:true});
				}
			}.bind(this),function(error){
				if(cb)
					cb(false);
				return toastr.error('Error de conexión','', {progressBar:true});
			});
		},
    setTokens: function(token,tokenSecret){
      return new Promise(function(resolve,reject){
        $cookies.put('token',token);
        $cookies.put('tokenSecret',tokenSecret);
        resolve(true);
      })
    },
		logintk: function(token, tokenSecret, back){
			$cookies.put('token',token);
			$cookies.put('tokenSecret',tokenSecret);
			$rootScope.$emit('logged', true);
			// if(back)
			// 	$window.history.back();
			// else
				// $state.go('base.index');
		},
		logout: function(){
			this.user = false;
			$http.get('http://50.21.179.35:8080/logout/'+$cookies.get('token')+'/'+$cookies.get('tokenSecret'))
			.then(function(){
				$cookies.remove("token");
				$cookies.remove("tokenSecret");
				$rootScope.$emit('logged', false);
				$state.go('base.index');
			},function(){
				$cookies.remove("token");
				$cookies.remove("tokenSecret");
				$rootScope.$emit('logged', false);
				// $state.go('base.index');
			});
		},
		validation: function(object,cb) {
			if(!object) {
				if(cb)
					cb(false);
				return 0;
			}

			$http.post('http://50.21.179.35:8082/users/validate/'+object)
			.then(function(res){
				if(res.data.e==0){
					cb(true);
				}
			},function(err) {
				if(cb)
					cb(false);
				return 0;
			});
		}
	};
	return object;
}]);
//GeoPosition
app.factory('GeoPosition', ['toastr', '$rootScope', function(toastr, $rootScope){
	return {
		geocoder: function(){
			if(google)
				return new google.maps.Geocoder();
			else
				return false;
		},
		oldAdd: '',
		geoPos: false,
		geocode: function(address, cb){
			if(address != this.oldAdd){
				this.oldAdd = address;
				this.geoPos = false;
				this.geocoder.geocode( { 'address': this.oldAdd}, function(results, status) {
					if (status == google.maps.GeocoderStatus.OK){
						$rootScope.$applyAsync(function(){
							this.geoPos = {
								lat: results[0].geometry.location.lat(),
								lng: results[0].geometry.location.lng()
							};
							return cb ? cb(true, angular.copy(this.geoPos)) : false;
						}.bind(this));
					}else
						return cb ? cb(false) : false;
				}.bind(this));
			}else if(geoPos)
				return cb ? cb(true) : false;
		},
		deg2rad: function(deg){
			return deg * (Math.PI/180);
		},
		distanceAlt: function(lat, lng, lat2, lng2){
			lat = angular.copy(parseFloat(lat));
			lng = angular.copy(parseFloat(lng));

			var R = 6371;
			var dLat = this.deg2rad(lat2 - lat);  // deg2rad below
			var dLon = this.deg2rad(lng2 - lng);
			var a =  Math.sin(dLat/2) * Math.sin(dLat/2) +
				Math.cos(this.deg2rad(lat)) * Math.cos(this.deg2rad(lat2)) *
				Math.sin(dLon/2) * Math.sin(dLon/2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			var d = R * c;

			/*if(isNaN(d))
				return "Desconocida";
			else if(d >= 1000)
				return Math.round(d / 1000) + 'Km';
			else
				return Math.round(d) + 'm';*/

			return d;
		},
		distance: function(lat, lng){
			lat = angular.copy(parseFloat(lat));
			lng = angular.copy(parseFloat(lng));
			if(!this.position)
				return "Desconocida";


			var R = 6371000;
			var dLat = this.deg2rad(this.position.lat - lat);  // deg2rad below
			var dLon = this.deg2rad(this.position.lng - lng);
			var a =  Math.sin(dLat/2) * Math.sin(dLat/2) +
				Math.cos(this.deg2rad(lat)) * Math.cos(this.deg2rad(this.position.lat)) *
				Math.sin(dLon/2) * Math.sin(dLon/2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			var d = R * c;

			if(isNaN(d))
				return "Desconocida";
			else if(d >= 1000)
				return Math.round(d / 1000) + 'Km';
			else
				return Math.round(d) + 'm';

			return d;
		},
		position: false,
		retrieve: function(position, cb){
			$rootScope.$applyAsync(function(){
				this.position = {
					lat: parseFloat(position.coords.latitude),
					lng: parseFloat(position.coords.longitude)
				};
				return cb ? cb(true) : false;
			}.bind(this));
		},
		request: function(cb){
			if(navigator.geolocation) {
				return navigator.geolocation.getCurrentPosition(function(position){this.retrieve(position, cb)}.bind(this));
			}else {
				toastr.error('Tu navegador no soporta esta función','', {progressBar:true});
				return cb ? cb(false) : false;
			}
		}
	}
}]);
//WebNotification
app.factory('WebNotification', [function(){
	var WNotification = {
		current : false,
		enabled : true,
		svC: false,
		send : function(title, options){
			console.log(title, options);
			if(this.current)
				current.close();
			try{
				current = new Notification(title, options);
			}catch(e){
				current = WNotification.svC.showNotification(title, options);
			}

		}
	};


	if (!("Notification" in window))
		WNotification.enabled = false;
	else if(Notification.permission === "granted"){
		WNotification.enabled = true;
		navigator.serviceWorker.register('javascripts/sw.js')
		.then(function(registration) {
			WNotification.svC = registration;
		});
	}else if(Notification.permission !== "denied"){
		Notification.requestPermission(function (permission) {
			if (permission === "granted"){
				WNotification.enabled = true;
				navigator.serviceWorker.register('javascripts/sw.js');
				navigator.serviceWorker.ready.then(function(registration) {
					WNotification.svC = registration;
				});
			}else
				WNotification.enabled = false;
		});
	}

	return WNotification;
}]);
//Notification
app.factory('Notification',['Auth','toastr','$q','$rootScope','$http', function(Auth, toastr, $q, $rootScope, $http){
	var object = {
		s: {},
		items: [],
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(cb){
			this.s.start = this.items.length;
			if(this.items.length >= this.total)
				return cb ? cb(false) : false;
			this.get(function(success,items){
				if(success)
					this.items = this.items.concat(items);
					if(cb)
						return cb ? cb(true) : false;
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar = $q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/notifications',
			{
				params:this.s,
				timeout:this.cancellar.promise
			})
			.then(function(response){
				this.cancellar = false;
				if(response.data.e == 0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar = false;
				return cb ? cb(false) : false;
			});
		},
	}
	return object;
}])
//Visibility
app.factory('Visibility', ['$rootScope', function($rootScope){
	var hidden,
		visibilityChange,
		Visibility = {
			isHidden: false,
			isItHidden: function(){
				return Visibility.isHidden;
			},
			change: function(){
				if (document[hidden]) {
					Visibility.isHidden = true;
					$rootScope.$emit('pageHidden');
				} else {
					Visibility.isHidden = false;
					$rootScope.$emit('pageShown');
				}
			}
		};

		if (typeof document.hidden !== "undefined") {
			hidden = "hidden";
			visibilityChange = "visibilitychange";
		} else if (typeof document.msHidden !== "undefined") {
			hidden = "msHidden";
			visibilityChange = "msvisibilitychange";
		} else if (typeof document.webkitHidden !== "undefined") {
			hidden = "webkitHidden";
			visibilityChange = "webkitvisibilitychange";
		}

		document.addEventListener(visibilityChange, Visibility.change, false);
		return Visibility;

}]);
//SocketChat
app.factory('SocketChat', ['$interval', 'toastr','$rootScope', '$cookies', 'Proposal', 'Message', 'GeoPosition', 'WebNotification', 'Visibility', function($interval, toastr, $rootScope, $cookies, Proposal, Message, GeoPosition, WebNotification, Visibility){
	var Socket = {
		messageOnCurrentChat: {},
		messageOnNewChat: {},
		a: {},
		online: false,
		socket: false,
		autoScroll: true,
		isTyping: false,
		startedHere : false,
		activeChat: {},
		interval: false,
		pingInterval: function(){
			if(this.online)
				this.socket.emit('pingg', {hi:true});
			else
				$interval.cancel(this.interval);
		},
		typing: function(){
			if(this.activeChat._id && !this.isTyping){
				this.isTyping = true;
				this.socket.emit('typing', {to: this.activeChat._id});
			}
		},
		stopTyping: function(){
			if(this.activeChat._id){
				this.isTyping = false;
				this.socket.emit('stoppedTyping', {to: this.activeChat._id});
			}
		},
		sendLocation: function(){
			if(this.activeChat._id)
				if(!GeoPosition.position)
					GeoPosition.request(function(success){
						if(success){
							var data = {lat: GeoPosition.position.lat, lng: GeoPosition.position.lng, to: this.activeChat._id};
							this.socket.emit('sendAddress', data);
						}else{
							toastr.error("No pudimos recuperar tu ubicación",'', {progressBar:true});
						}
					}.bind(this));
				else{
					var data = {lat: GeoPosition.position.lat, lng: GeoPosition.position.lng, to: this.activeChat._id};
					this.socket.emit('sendAddress', data);
				}
			else
				toastr.error("Debes seleccionar un chat",'', {progressBar:true});
		},
		sendMessage: function(){
			this.a.message = this.a.message.trim();
			if(this.a.message.length > 0){
				this.socket.emit('mmessage', this.a);
				this.a.message = "";
			}
		},
		changeActive: function(proposal){
			if(proposal._id == this.activeChat._id) return;
			this.stopTyping();
			for(var i = 0; i < Proposal.items; i++)
				Proposal[i].active = false;
			this.activeChat.active = false;
			this.activeChat = proposal;
			this.activeChat.active = true;
			this.autoScroll = true;
			this.a = {
				to: this.activeChat._id
			};
			Message.items = [];
			Message.endReached = false;
			Message.get(this.activeChat);
			this.activeChat.unreadU = 0;
		},
		startChat: function(house){
			this.startedHere = true;
			this.socket.emit('startChat', {to: house._id});
		},
		onTyping: function(data){
			for(var i = 0; i < Proposal.items.length ; i++){
				if(Proposal.items[i]._id == data.proposal){
					$rootScope.$applyAsync(function (){
						Proposal.items[i].typing = true;
					}.bind(this));
					break;
				}
			}
		},
		onStoppedTyping: function(data){
			for(var i = 0; i < Proposal.items.length ; i++){
				if(Proposal.items[i]._id == data.proposal){
					$rootScope.$applyAsync(function (){
						Proposal.items[i].typing = false;
					}.bind(this));
					break;
				}
			}
		},
		toYou: function(data){
			$rootScope.$applyAsync(function (){

				if(Visibility.isItHidden()){
					WebNotification.send("Nuevo Mensaje", {body: data.message, icon: "https://uploads.qllevo.com/200/isologo.png", image: "https://uploads.qllevo.com/200/"+data.user.photo, vibrate: [200]});
				}

				var newOrder = [], start = 1, i, found = false;
				this.messageOnNewChat.volume= 1;
				this.messageOnCurrentChat.volume= 1;

				for(i = 0; i < Proposal.items.length ; i++){
					if(Proposal.items[i]._id == data.proposal){
						found = true;
						newOrder[0] = Proposal.items[i];
					}else{
						newOrder[start] = Proposal.items[i];
						start++;
					}
				}

				if(found){
					Proposal.items = angular.copy(newOrder);
					if(data.proposal == this.activeChat._id){
						Message.items.unshift(data);
						this.messageOnCurrentChat.play();
					}else{
						this.messageOnNewChat.play();
						Proposal.items[0].unreadU++;
					}
				}else{
					this.messageOnNewChat.play();
					this.newChat();
				}

			}.bind(this));
		},
		byYou: function(data){
			$rootScope.$applyAsync(function () {

				var newOrder = [], start = 1, i, found = false;
				for(i = 0; i < Proposal.items.length ; i++){
					if(Proposal.items[i]._id == data.proposal){
						found = true;
						newOrder[0] = Proposal.items[i];
					}else{
						newOrder[start] = Proposal.items[i];
						start++;
					}
				}

				if(found){
					Proposal.items = angular.copy(newOrder);
					if(data.proposal == this.activeChat._id)
						Message.items.unshift(data);
				}else
					this.newChat();

			}.bind(this));
		},
		newChat: function(data){
			Proposal.pageChange(1, function(success){
				if(success && (this.startedHere || (Proposal.items.length > 0 && !this.activeChat._id))){
					this.changeActive(Proposal.items[0]);
				}
				this.startedHere = false;
				$('#chatBody').collapse("show")
			}.bind(this));
		},
		onConnect: function(){

			$rootScope.$emit('chatConnected',true);
			Proposal.get(function(success){
				this.online = true;
				if(success && Proposal.items.length > 0 && !this.activeChat._id)
					this.changeActive(Proposal.items[0]);

				this.interval = $interval(this.pingInterval.bind(this),5000);

			}.bind(this));
		},
		onDisconnect: function(){
			$rootScope.$applyAsync(function () {
				this.online = false;

				$interval.cancel(this.interval);
				$rootScope.$emit('chatConnected',false);
			}.bind(this));
		},
		prepareAudio: function(){

			function removeBehaviorsRestrictions() {
				Socket.messageOnCurrentChat.play().then(function(){
					document.querySelector('body').removeEventListener('keydown', removeBehaviorsRestrictions);
					document.querySelector('body').removeEventListener('mousedown', removeBehaviorsRestrictions);
					document.querySelector('body').removeEventListener('ontouchmove', removeBehaviorsRestrictions);
					document.querySelector('body').removeEventListener('ontouchend', removeBehaviorsRestrictions);
					document.querySelector('body').removeEventListener('ontouchcancel', removeBehaviorsRestrictions);
					document.querySelector('body').removeEventListener('ontouchstart', removeBehaviorsRestrictions);
					document.querySelector('body').removeEventListener('click', removeBehaviorsRestrictions);
					//Socket.messageOnCurrentChat.volume = 1;
				}, function(){});

				Socket.messageOnNewChat.play().then(function(){
					document.querySelector('body').removeEventListener('keydown', removeBehaviorsRestrictions);
					document.querySelector('body').removeEventListener('mousedown', removeBehaviorsRestrictions);
					document.querySelector('body').removeEventListener('ontouchmove', removeBehaviorsRestrictions);
					document.querySelector('body').removeEventListener('ontouchend', removeBehaviorsRestrictions);
					document.querySelector('body').removeEventListener('ontouchcancel', removeBehaviorsRestrictions);
					document.querySelector('body').removeEventListener('ontouchstart', removeBehaviorsRestrictions);
					document.querySelector('body').removeEventListener('click', removeBehaviorsRestrictions);
					//Socket.messageOnNewChat.volume = 1;
				}, function(){});
			}

			document.querySelector('body').addEventListener('keydown', removeBehaviorsRestrictions);
			document.querySelector('body').addEventListener('mousedown', removeBehaviorsRestrictions);
			document.querySelector('body').addEventListener('ontouchmove', removeBehaviorsRestrictions);
			document.querySelector('body').addEventListener('ontouchend', removeBehaviorsRestrictions);
			document.querySelector('body').addEventListener('ontouchcancel', removeBehaviorsRestrictions);
			document.querySelector('body').addEventListener('ontouchstart', removeBehaviorsRestrictions);
			document.querySelector('body').addEventListener('click', removeBehaviorsRestrictions);

		},
		build: function(){
			this.messageOnCurrentChat = new Audio("/assets/audio/currentChat.ogg");
			this.messageOnCurrentChat.type= 'audio/ogg';
			this.messageOnCurrentChat.src= "/assets/audio/currentChat.ogg";
			this.messageOnCurrentChat.volume= 0;

			this.messageOnNewChat = new Audio("/assets/audio/newChat.ogg");
			this.messageOnNewChat.type= 'audio/ogg';
			this.messageOnNewChat.src= "/assets/audio/newChat.ogg";
			this.messageOnNewChat.volume= 0;

			this.prepareAudio();
			this.socket = io.connect('//chat.qllevo.com', { query: "token="+$cookies.get('token')+"&tokensecret="+$cookies.get('tokenSecret')});
			this.socket.on('connect', this.onConnect.bind(this));
			this.socket.on('toYou', this.toYou.bind(this));
			this.socket.on('byYou', this.byYou.bind(this));
			this.socket.on('newChat', this.newChat.bind(this));
			this.socket.on('disconnect', this.onDisconnect.bind(this));
			this.socket.on('typing', this.onTyping.bind(this));
			this.socket.on('stoppedTyping', this.onStoppedTyping.bind(this));
		},
		disconnect: function(){
			if(this.socket)
				this.socket.disconnect();
		}

	};

	return Socket;
}]);
//Socket
app.factory('Socket', ['$interval', 'toastr','$rootScope', '$cookies', 'GeoPosition', function($interval, toastr, $rootScope, $cookies, GeoPosition){
	var Socket = {
		online: false,
		socket: false,
		items: [],
		interval: false,
		clean: function(){
			for(var i = 0; i < this.items.length; i++){
				if(this.items[i].time < Date.now() - 1 * 60 * 1000)
					this.items.splice(i,1);
			}
		},
		newPos: function(data){
			$rootScope.$applyAsync(function () {

				var add = true;
				for(var i = 0; i < this.items.length; i++)
					if(data.user._id == this.items[i].user._id){
						add = false;
						this.items[i].time = data.time;
						this.items[i].lat = data.lat;
						this.items[i].lng = data.lng;
					}

				if(add)
					this.items.unshift(data);

			}.bind(this));

		},
		onConnect: function(){
			$rootScope.$applyAsync(function () {
				this.online = true;
				this.interval = $interval(this.clean.bind(this),10000);
			}.bind(this));
		},
		onDisconnect: function(){
			$rootScope.$applyAsync(function () {
				this.online = false;
				$interval.cancel(this.interval);
			}.bind(this));
		},
		build: function(){
			this.socket = io.connect('//tracker.qllevo.com/', { secure: true, transports: [ "websocket" ], query: "token="+$cookies.get('token')+"&tokensecret="+$cookies.get('tokenSecret')});
			this.socket.on('connect', this.onConnect.bind(this));
			this.socket.on('newPosA', this.newPos.bind(this));
			this.socket.on('disconnect', this.onDisconnect.bind(this));
		},
		disconnect: function(){
			if(this.socket)
				this.socket.disconnect();
		}

	};

	return Socket;
}]);
//Users
app.factory('Users', ['Auth','toastr','$q','$rootScope','$http', 'Upload', function(Auth,toastr,$q,$rootScope,$http,Upload){
	var object = {
		s: {},
		p: {},
		a: {},
		d: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar = $q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/users/searchp',
			{
				params:this.s,
				timeout:this.cancellar.promise
			})
			.then(function(response){
				this.cancellar = false;
				if(response.data.e == 0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar = false;
				return cb ? cb(false) : false;
			});
		},
		getSingle: function(params,cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar = $q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/users/id/'+params,
			{
				params: this.s,
				timeout:this.cancellar.promise
			})
			.then(function(response){
				this.cancellar = false;
				if(response.data.e == 0){
					if(!skip)
						this.item = angular.copy(response.data.item);
					this.total = response.data.total;
					return cb ? cb(true, response.data.item) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar = false;
				return cb ? cb(false) : false;
			});
		},
		getSelf: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar = $q.defer();

			var ss = {};
			if(Auth.isLogged()){
				ss.token = Auth.getToken();
				ss.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.get('http://50.21.179.35:8082/users',{
				params:ss
			}).then(function(response){
				if(response.data.e == 0){
					this.item = response.data.item;
					return cb ? cb(true,response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			// if(Auth.isLogged()){
			// 	this.a.token = Auth.getToken();
			// 	this.a.tokensecret = Auth.getTokenSecret();
			// }else{
			// 	if(cb) cb(false);
			// 	return toastr.error("Debes estar logueado",'', {progressBar:true});
			// }

			var promise;
			if(this.a.photo.name){
				promise = Upload.upload({
					url: 'http://50.21.179.35:8082/users',
					arrayKey: '',
					data: this.a,
					method: 'POST'
				})
			}else{
				promise =  $http.post('http://50.21.179.35:8082/users',this.a);
			}

			promise.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(cb){
			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debe estar logueado",'',{progressBar:true});
			}
			var promise;
			if((this.p.photo64) || (this.p.cover64)){
				promise = Upload.upload({
					url:'http://50.21.179.35:8082/users',
					arrayKey: '',
					data: this.p,
					method: 'PUT'
				})
			}else{
				promise = $http.put('http://50.21.179.35:8082/users',this.p);
			}

			promise.then(function(response){
				if(response.data.e == 0){
          if(response.data.token && response.data.tokensecret)
            return cb ? cb(response.data): false;
          else
            return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		validate: function(cb){

			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debe estar logueado",'',{progressBar:true});
			}

			 $http.put('http://50.21.179.35:8082/users/validateuser/'+this.p._id, this.p)
			 .then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		putSelf: function(params,cb){
			if(Auth.isLogged()){
				params.token = Auth.getToken();
				params.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debe estar logueado",'',{progressBar:true});
			}

			var promise;
			if((this.p.photo64) || (this.p.cover64)){
				promise = Upload.upload({
					url:'http://50.21.179.35:8082/users?token='+Auth.getToken()+'&tokensecret='+Auth.getTokenSecret(),
					data: params,
					method: 'PUT'
				})
			}else{
				promise = $http.put('http://50.21.179.35:8082/users?token='+Auth.getToken()+'&tokensecret='+Auth.getTokenSecret(),params);
			}

			promise.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			})
		},
		remove: function(cb){
			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}
			$http.delete('http://50.21.179.35:8082/users/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setPut: function(item){
			this.p = angular.copy(item);
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			  '¿Esta Seguro de Eliminar?',
			  {
			   progressBar:true,
			   timeOut: 0,
			   extendedTimeOut: 5000,
			   closeButton: true,
			   allowHtml: true,
			   onTap: function(){
				   this.remove(function(success){
					   if(success){
						   toastr.success('Datos Eliminados','',{progressBar:true});
						   this.get();
					   }
					}.bind(this));
				  }.bind(this)
			  });
		}
	};
	return object;
}]);
//Banks
app.factory('Banks',['Auth','toastr','$q','$rootScope','$http',function(Auth,toastr,$q,$rootScope,$http){
	var object = {
		s: {},
		p: {},
		a: {},
		d: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/banks/search',
			{
				params:this.s,
				timeout:this.cancellar.promise
			})
			.then(function(response){
				this.cancellar=false;
				if(response.data.e==0){
					// if(!skip)
					// 	this.items = angular.copy(response.data.items);
					this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){

			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/banks',this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(cb){

			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.put('http://50.21.179.35:8082/banks/id/'+this.p._id,this.p).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){

			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}
			$http.delete('http://50.21.179.35:8082/banks/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setPut: function(item){
			this.p = angular.copy(item);
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			  '¿Esta Seguro de Eliminar?',
			  {
			   progressBar:true,
			   timeOut: 0,
			   extendedTimeOut: 5000,
			   closeButton: true,
			   allowHtml: true,
			   onTap: function(){
				   this.remove(function(success){
					   if(success){
						   toastr.success('Datos Eliminados','',{progressBar:true});
						   this.get();
					   }
					}.bind(this));
				  }.bind(this)
			  });
		}
	};
	return object;
}]);
//Banks Account
app.factory('BanksAccount',['Auth','toastr','$q','$rootScope','$http',function(Auth,toastr,$q,$rootScope,$http){
	var object = {
		s: {},
		p: {},
		a: {},
		d: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/bankaccounts/search',
			{
				params:this.s,
				timeout:this.cancellar.promise
			})
			.then(function(response){
				this.cancellar=false;
				if(response.data.e==0){
					// if(!skip)
					// 	this.items = angular.copy(response.data.items);
					this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){

			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/bankaccounts',this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(cb){

			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.put('http://50.21.179.35:8082/bankaccounts/id/'+this.p._id,this.p).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){

			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}
			$http.delete('http://50.21.179.35:8082/bankaccounts/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setPut: function(item){
			this.p = angular.copy(item);
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			  '¿Esta Seguro de Eliminar?',
			  {
			   progressBar:true,
			   timeOut: 0,
			   extendedTimeOut: 5000,
			   closeButton: true,
			   allowHtml: true,
			   onTap: function(){
				   this.remove(function(success){
					   if(success){
						   toastr.success('Datos Eliminados','',{progressBar:true});
						   this.get();
					   }
					}.bind(this));
				  }.bind(this)
			  });
		}
	};
	return object;
}]);
//banners
app.factory('Banners',['Auth','toastr','$q','$rootScope','$http','Upload',function(Auth,toastr,$q,$rootScope,$http,Upload){
	var object = {
		s: {},
		a: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/banners/search', {params:this.s, timeout:this.cancellar.promise})
			.then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			var promise;
			if(this.a.file && this.a.file.name){
				promise = Upload.upload({
					url: 'http://50.21.179.35:8082/banners?token='+Auth.getToken()+'&tokensecret='+Auth.getTokenSecret(),
					arrayKey: '',
					data: this.a,
					method: 'POST'
				})
			}else{
				promise = $http.post('http://50.21.179.35:8082/banners',this.a)
			}

			promise.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(cb){
			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			var promise;
			if(this.p.file && this.p.file.name){
				promise = Upload.upload({
					url: 'http://50.21.179.35:8082/banners/id/'+this.p._id+'?token='+Auth.getToken()+'&tokensecret='+Auth.getTokenSecret(),
					arrayKey: '',
					data: this.p,
					method: 'PUT'
				})
			}else{
				promise = $http.put('http://50.21.179.35:8082/banners/id/'+this.p._id,this.p)
			}

			promise.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){
			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			$http.delete('http://50.21.179.35:8082/banners/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setPut: function(item){
			this.p = angular.copy(item);
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			'¿Esta seguro de Eliminar?',
			{
				progressBar:true,
				timeOut: 5000,
				closeButton: true,
				allowHtml:true,
				onTap: function(){
					this.remove(function(success){
						if(success){
							toastr.success('Datos Eliminados','',{progressBar:true});
							this.get();
						}
					}.bind(this));
				}.bind(this)
			});
		}
	};
	return object;
}]);
//categories
app.factory('Category',['Auth','toastr','$q','$rootScope','$http','Upload',function(Auth,toastr,$q,$rootScope,$http,Upload){
	var object = {
		s: {},
		p: {},
		a: {},
		d: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			// if(this.cancellar)
			// 	this.cancellar.resolve();
			// this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/categories/search', {params:this.s, timeout:this.cancellar.promise})
			.then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else {
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		getSingle: function(cb, skip){
			// if(this.cancellar)
			// 	this.cancellar.resolve();
			// this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/categories/id/'+this.s._id, {timeout:this.cancellar.promise})
			.then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.item = angular.copy(response.data.item);
					this.total = response.data.total;
					return cb ? cb(true, response.data.item) : false;
				}else {
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			var promise;
			if(this.a.file && this.a.file.name){
				promise = Upload.upload({
					url: 'http://50.21.179.35:8082/categories?token='+Auth.getToken()+'&tokensecret='+Auth.getTokenSecret(),
					arrayKey: '',
					data: this.a,
					method: 'POST'
				})
			}else{
				promise = $http.post('http://50.21.179.35:8082/categories',this.a)
			}

			promise.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		parents: function(array, id){
			for(var i = 0; i < array.length; i++){
				if(array[i]._id == id)
					return array[i];
				else if(Array.isArray(array[i].childs)){
					var j = this.parents(array[i].childs, id);
					if(j)
						return j;
				}
			}
		},
		getChilds: function(id,cb){
			$http.get('http://50.21.179.35:8082/categories/search',
			{
				params:{parent:id},
				timeout:this.cancellar.promise
			})
			.then(function(response){
				if(response.data.e == 0){
					var t = this.parents(this.items, id);
					cb ? cb(true, response.data.items) : false;
					t['childs'] = angular.copy(response.data.items);
					return;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(cb){
			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			var promise;
			if(this.p.file && this.p.file.name){
				promise = Upload.upload({
					url: 'http://50.21.179.35:8082/categories/id/'+this.p._id+'?token='+Auth.getToken()+'&tokensecret='+Auth.getTokenSecret(),
					arrayKey: '',
					data: this.p,
					method: 'PUT'
				})
			}else{
				promise = $http.put('http://50.21.179.35:8082/categories/id/'+this.p._id,this.p)
			}

			promise.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){
			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			$http.delete('http://50.21.179.35:8082/categories/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setPut: function(item){
			this.p = angular.copy(item);
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			'¿Esta seguro de Eliminar?',
			{
				progressBar:true,
				timeOut: 5000,
				closeButton: true,
				allowHtml:true,
				onTap: function(){
					this.remove(function(success){
						if(success){
							toastr.success('Datos Eliminados','',{progressBar:true});
							this.get();
						}
					}.bind(this));
				}.bind(this)
			});
		}
	};
	return object;
}]);
//faqs
app.factory('Faqs',['Auth','toastr','$q','$rootScope','$http',function(Auth,toastr,$q,$rootScope,$http){
	var object = {
		s: {},
		p: {},
		a: {},
		d: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/faqs/search', {params:this.s, timeout:this.cancellar.promise})
			.then(function(response) {
				this.cancellar=false;
				if(response.data.e==0) {
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else {
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error) {
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){

			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/faqs',this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(cb){

			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.put('http://50.21.179.35:8082/faqs/id/'+this.p._id,this.p)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){

			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}
			$http.delete('http://50.21.179.35:8082/faqs/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setPut: function(item){
			console.log(this.p.categoriesfaq);
			this.p = angular.copy(item);
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			  '¿Esta Seguro de Eliminar?',
			  {
			   progressBar:true,
			   timeOut: 0,
			   extendedTimeOut: 5000,
			   closeButton: true,
			   allowHtml: true,
			   onTap: function(){
				   this.remove(function(success){
					   if(success){
						   this.get();
					   }
					}.bind(this));
				  }.bind(this)
			  });
		}
	};
	return object;
}]);
//categoriesfaq
app.factory('Categoryfaqs',['Auth','toastr','$q','$rootScope','$http','Upload', function(Auth,toastr,$q,$rootScope,$http,Upload){
	var object = {
		s: {},
		p: {},
		a: {},
		d: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/categoriesfaq/search',
			{
				params:this.s,
				timeout:this.cancellar.promise
			})
			.then(function(response) {
				this.cancellar=false;
				if(response.data.e==0) {
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else {
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error) {
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){

			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/categoriesfaq',this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		parents: function(array, id){
			for(var i = 0; i < array.length; i++){
				if(array[i]._id == id)
					return array[i];
				else if(Array.isArray(array[i].childs)){
					var j = this.parents(array[i].childs, id);
					if(j)
						return j;
				}
			}
		},
		getChilds: function(id,cb){
			$http.get('http://50.21.179.35:8082/categoriesfaq/search',
			{
				params:{parent:id},
				timeout:this.cancellar.promise
			})
			.then(function(response){
				if(response.data.e == 0){
					var t = this.parents(this.items, id);
					cb ? cb(true, response.data.items) : false;
					t['childs'] = angular.copy(response.data.items);
					return;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(cb){

			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.put('http://50.21.179.35:8082/categoriesfaq/id/'+this.p._id,this.p)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){

			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}
			$http.delete('http://50.21.179.35:8082/categoriesfaq/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setPut: function(item){
			this.p = angular.copy(item);
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			  '¿Esta Seguro de Eliminar?',
			  {
			   progressBar:true,
			   timeOut: 0,
			   extendedTimeOut: 5000,
			   closeButton: true,
			   allowHtml: true,
			   onTap: function(){
				   this.remove(function(success){
					   if(success){
						   toastr.success('Datos Eliminados','',{progressBar:true});
						   this.get();
					   }
					}.bind(this));
				  }.bind(this)
			  });
		}
	};
	return object;
}]);
//Locality
app.factory('Locality', ['Auth','toastr','$q','$rootScope','$http', function(Auth,toastr,$q,$rootScope,$http){
	var object = {
		s: {},
		p: {},
		a: {},
		d: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar = $q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/localities/search',
			{
				params:this.s,
				timeout:this.cancellar.promise
			})
			.then(function(response){
				this.cancellar = false;
				if(response.data.e == 0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar = false;
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		getSingle: function(cb){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar = $q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/localities/id/'+this.s._id,
			{
				params:this.s,
				timeout:this.cancellar.promise
			})
			.then(function(response){
				this.cancellar = false;
				if(response.data.e == 0){
					this.item = angular.copy(response.data.item);
					this.total = response.data.total;
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar = false;
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		parents: function(array, id){
			for(var i = 0; i < array.length; i++){
				if(array[i]._id == id)
					return array[i];
				else if(Array.isArray(array[i].childs)){
					var j = this.parents(array[i].childs, id);
					if(j)
						return j;
				}
			}
		},
		getChilds: function(id,cb){
			$http.get('http://50.21.179.35:8082/localities/search',
			{
				params:{parent:id},
				timeout:this.cancellar.promise
			})
			.then(function(response){
				if(response.data.e == 0){
					var t = this.parents(this.items, id);
					cb ? cb(true, response.data.items) : false;
					t['childs'] = angular.copy(response.data.items);
					return;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/localities',this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(cb){
			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debe estar logueado",'',{progressBar:true});
			}

			$http.put('http://50.21.179.35:8082/localities/id/'+this.p._id,this.p)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){
			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}
			$http.delete('http://50.21.179.35:8082/localities/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setPut: function(item){
			this.p = angular.copy(item);
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			  '¿Esta Seguro de Eliminar?',
			  {
			   progressBar:true,
			   timeOut: 0,
			   extendedTimeOut: 5000,
			   closeButton: true,
			   allowHtml: true,
			   onTap: function(){
				   this.remove(function(success){
					   if(success){
						   toastr.success('Datos Eliminados','',{progressBar:true});
						   this.get();
					   }
					}.bind(this));
				  }.bind(this)
			  });
		}
	};
	return object;
}]);
//publications
app.factory('Publications',['Auth','toastr','$q','$rootScope','$http','Upload',function(Auth,toastr,$q,$rootScope,$http,Upload){
	var object = {
		s: {},
		p: {},
		a: {},
		d: {},
		items: [],
		itemSearch: [],
		item: {},
		total: 0,
		totalSearch: 0,
		cancellar: false,
		cancellarSearch: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip, state = true){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/publications/search', {params:this.s, timeout:this.cancellar.promise})
			.then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);

					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		getSearch: function(cb, skip){
			if(this.cancellarSearch)
				this.cancellarSearch.resolve();
			this.cancellarSearch=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/publications/predictiveSearch', {params:this.s, timeout:this.cancellar.promise})
			.then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.itemSearch = angular.copy(response.data.items);
					this.totalSearch = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		getSingle: function(params,cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/publications/id/'+params, {params:this.s, timeout:this.cancellar.promise})
			.then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.item = angular.copy(response.data.item);
					this.total = response.data.total;
					return cb ? cb(true, response.data.item) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){

			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}
			var promise;
			console.log(this.a);

			if(this.a.photos && this.a.photos[0].name){
				promise = Upload.upload({
					url:  'http://50.21.179.35:8082/publications?token='+Auth.getToken()+'&tokensecret='+Auth.getTokenSecret(),
					arrayKey: '',
					data: this.a,
					method: 'POST'
				})
			}else{
				promise = $http.post('http://50.21.179.35:8082/publications',this.a)
			}

			promise.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true,angular.copy(response.data.id)) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				console.log(error);
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(cb){

			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			var promise;
			if(this.p.files && this.p.files[0].name){
				promise = Upload.upload({
					url:  'http://50.21.179.35:8082/publications/id/'+this.p._id+'?token='+Auth.getToken()+'&tokensecret='+Auth.getTokenSecret(),
					arrayKey: '',
					data: this.p,
					method: 'PUT'
				})
			}else{
				promise = $http.put('http://50.21.179.35:8082/publications/id/'+this.p._id,this.p)
			}

			promise.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){

			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}
			$http.delete('http://50.21.179.35:8082/publications/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setPut: function(item){
			this.p = angular.copy(item);

			for(var key in this.p.categories)
				this.p.categories[key] = this.p.categories[key]._id;

			delete this.p.user;
			delete this.p.localities;

			var startdate = new Date(this.p.startdate);
			this.p.startdate = 	startdate.getDate()+"-"+(startdate.getMonth()+1)+"-"+startdate.getFullYear();
			//this.p.pickup.hourf = 	pickupf.getHours()+":"+pickupf.getMinutes();
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			  '¿Esta Seguro de Eliminar?',
			  {
			   progressBar:true,
			   timeOut: 0,
			   extendedTimeOut: 5000,
			   closeButton: true,
			   allowHtml: true,
			   onTap: function(){
				   this.remove(function(success){
					   if(success){
						   toastr.success('Datos Eliminados','',{progressBar:true});
						   this.get();
					   }
					}.bind(this));
				  }.bind(this)
			  });
		}
	};
	return object;
}]);
//reportcomment
app.factory('ReportComments',['Auth','toastr','$q','$rootScope','$http',function(Auth,toastr,$q,$rootScope,$http){
	var object = {
		s: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/reportcomments/search', {params:this.s, timeout:this.cancellar.promise})
			.then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		}
	};
	return object;
}]);
//report
app.factory('Reports',['Auth','toastr','$q','$rootScope','$http',function(Auth,toastr,$q,$rootScope,$http){
	var object = {
		s: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();


			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			let url;

			if(this.s.byme)
				url = 'http://50.21.179.35:8082/reports/byme';
			if(this.s.forme)
				url = 'http://50.21.179.35:8082/reports/forme';

			url = url ? url : 'http://50.21.179.35:8082/reports/search';

			$http.get(url, {params:this.s, timeout:this.cancellar.promise})
			.then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/reports/open/'+this.a._id,this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true,angular.copy(response.data.id)) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
	};
	return object;
}]);
//review
app.factory('Reviews',['Auth','toastr','$q','$rootScope','$http',function(Auth,toastr,$q,$rootScope,$http){
	var object = {
		s: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/reviews/search', {params:this.s, timeout:this.cancellar.promise})
			.then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		}
	};
	return object;
}]);
//transaction
app.factory('Transactions',['Auth','toastr','$q','$rootScope','$http','Upload',function(Auth,toastr,$q,$rootScope,$http,Upload){
	var object = {
		s: {},
		a: {},
		p: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		getSingle: function(cb,skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar = $q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/transactions/id/'+this.s._id,
			{
				// params: this.s,
				timeout:this.cancellar.promise
			})
			.then(function(response){
				this.cancellar = false;
				if(response.data.e == 0){
					if(!skip)
						this.item = angular.copy(response.data.item);
					this.total = response.data.total;
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar = false;
				return cb ? cb(false) : false;
			});
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}
			let url;

			if(this.s.sales)
				url = 'http://50.21.179.35:8082/transactions/sales';
			if(this.s.purchases)
				url = 'http://50.21.179.35:8082/transactions/purchases';

			url = url ? url : 'http://50.21.179.35:8082/transactions/search';

			console.log(url)

			$http.get(url, {params:this.s, timeout:this.cancellar.promise})
			.then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/transactions/buy/'+this.a._id,this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true,angular.copy(response.data.id)) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(options,cb){
			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debe estar logueado",'',{progressBar:true});
			}

			let url;

			switch(options){
				case 0:
					url = "http://50.21.179.35:8082/transactions/addPayment/";
					break;
				case 1:
					url = "http://50.21.179.35:8082/transactions/addBilling/";
					break;
				case 2:
					url = "http://50.21.179.35:8082/transactions/addDelivery/";
					break;
			}

			var promise;
			if(this.p.file){
				promise = Upload.upload({
					url:url+this.p._id+'?token='+Auth.getToken()+'&tokensecret='+Auth.getTokenSecret(),
					arrayKey: '',
					data: this.p,
					method: 'PUT'
				})
			}else{
				promise = $http.put(url+this.p._id,this.p);
			}

			promise.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
	};
	return object;
}]);
// Rate
app.factory('Rate',['Auth','toastr','$q','$rootScope','$http','Upload', (Auth,toastr,$q,$rootScope,$http,Upload) => {
	return {
		s: {},
		a: {},
		p: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		post: function(cb){
			console.log('here')
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/transactions/addRate/'+this.a._id,this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true,angular.copy(response.data.id)) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		}
	}
}])
//panaPagos
app.factory('PanaPagos',['Auth','toastr','$q','$rootScope','$http','Upload',function(Auth,toastr,$q,$rootScope,$http,Upload){
	var object = {
		s: {},
		a: {},
		p: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/panaPagos/search', {params:this.s, timeout:this.cancellar.promise})
			.then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}
			var promise;
			if(this.a.file){
				promise = Upload.upload({
					url:'http://50.21.179.35:8082/panaPagos/pay/'+this.a._id+'?token='+Auth.getToken()+'&tokensecret='+Auth.getTokenSecret(),
					arrayKey: '',
					data: this.a,
					method: 'POST'
				})
			}else{
				promise = $http.post('http://50.21.179.35:8082/panaPagos/pay/'+this.a._id,this.a);
			}

			promise.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true,angular.copy(response.data.id)) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		}
	};
	return object;
}]);
//valuepairs
app.factory('Valuepair', ['Auth','toastr','$q','$rootScope','$http', function(Auth,toastr,$q,$rootScope,$http){
	var object = {
		s: {},
		p: {},
		a: {},
		d: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar = $q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/valuepairs/search',
			{
				params:this.s,
				timeout:this.cancellar.promise
			})
			.then(function(response){
				this.cancellar=false;
				if(response.data.e == 0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar = false;
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/valuepairs',this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(cb){
			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debe estar logueado",'',{progressBar:true});
			}

			$http.put('http://50.21.179.35:8082/valuepairs/id/'+this.p._id,this.p)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){
			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}
			$http.delete('http://50.21.179.35:8082/valuepairs/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setPut: function(item){
			this.p = angular.copy(item);
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			  '¿Esta Seguro de Eliminar?',
			  {
			   progressBar:true,
			   timeOut: 0,
			   extendedTimeOut: 5000,
			   closeButton: true,
			   allowHtml: true,
			   onTap: function(){
				   this.remove(function(success){
					   if(success){
						   this.get();
					   }
					}.bind(this));
				  }.bind(this)
			  });
		}
	};
	return object;
}]);
//Spots
app.factory('Spots',['Auth','toastr','$q','$rootScope','$http',function(Auth,toastr,$q,$rootScope,$http){
	var object = {
		s: {},
		a: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/spots/search', {
				params:this.s, timeout:this.cancellar.promise
			}).then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/spots',this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(cb){
			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}


			$http.put('http://50.21.179.35:8082/banners/id/'+this.p._id,this.p)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){
			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			$http.delete('http://50.21.179.35:8082/spots/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setPut: function(item){
			this.p = angular.copy(item);
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			'¿Esta seguro de Eliminar?',
			{
				progressBar:true,
				timeOut: 5000,
				closeButton: true,
				allowHtml:true,
				onTap: function(){
					this.remove(function(success){
						if(success){
							toastr.success('Datos Eliminados','',{progressBar:true});
							this.get();
						}
					}.bind(this));
				}.bind(this)
			});
		}
	};
	return object;
}]);
//views
app.factory('Views',['Auth','toastr','$q','$rootScope','$http',function(Auth,toastr,$q,$rootScope,$http){
	var object = {
		s: {},
		a: {},
		b: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/views/search', {
				params:this.s, timeout:this.cancellar.promise
			}).then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/views',this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(cb){
			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}
			$http.put('http://50.21.179.35:8082/views/id/'+this.p._id,this.p)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){
			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			$http.delete('http://50.21.179.35:8082/views/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setPut: function(item){
			this.p = angular.copy(item);
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			'¿Esta seguro de Eliminar?',
			{
				progressBar:true,
				timeOut: 5000,
				closeButton: true,
				allowHtml:true,
				onTap: function(){
					this.remove(function(success){
						if(success){
							toastr.success('Datos Eliminados','',{progressBar:true});
							this.get();
						}
					}.bind(this));
				}.bind(this)
			});
		}
	};
	return object;
}]);
//Friendships
app.factory('Friendships',['Auth','toastr','$q','$rootScope','$http',function(Auth,toastr,$q,$rootScope,$http){
	var object = {
		s: {},
		a: {},
		d: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/friendships', {
				params:this.s, timeout:this.cancellar.promise
			}).then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		getSingle: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/friendships/user/'+params, {
				params:this.s, timeout:this.cancellar.promise
			}).then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.item = angular.copy(response.data.item);
					this.total = response.data.total;
					return cb ? cb(true, response.data.item) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		getSelf: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar = $q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			let url;
			if(this.s.fromMe)
				url = 'http://50.21.179.35:8082/friendrequests/fromMe';
			if(this.s.toMe)
				url = 'http://50.21.179.35:8082/friendrequests/toMe';

			if(!this.s.fromMe || !this.s.toMe)
				url = 'http://50.21.179.35:8082/friendships';

			$http.get(url,{params: this.s}).then(function(response){
				if(response.data.e == 0){
					this.item = response.data.item;
					return cb ? cb(true,response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}
			let url;
			if(this.a._Idfriendship)
				url = 'http://50.21.179.35:8082/friendrequests/request/'+this.a._Idfriendship;
			if(this.a._Idfriendship_success)
				url = 'http://50.21.179.35:8082/friendrequests/accept/'+this.a._Idfriendship_success;
			if(this.a._Idfriendship_cancel)
				url = 'http://50.21.179.35:8082/friendrequests/accept/'+this.a._Idfriendship_cancel;

			$http.post(url,this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){
			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			$http.delete('http://50.21.179.35:8082/friendships/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			'¿Esta seguro de Eliminar?',
			{
				progressBar:true,
				timeOut: 5000,
				closeButton: true,
				allowHtml:true,
				onTap: function(){
					this.remove(function(success){
						if(success){
							toastr.success('Datos Eliminados','',{progressBar:true});
							this.get();
						}
					}.bind(this));
				}.bind(this)
			});
		}
	}
	return object;
}])
//followers
app.factory('Follows',['Auth','toastr','$q','$rootScope','$http',function(Auth,toastr,$q,$rootScope,$http){
	var object = {
		s: {},
		a: {},
		d: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			let url;

			if(this.s.follows)
				url = 'http://50.21.179.35:8082/follows/follows/'+this.s._id;
			if(this.s.followers)
				url = 'http://50.21.179.35:8082/follows/followers/'+this.s._id;

			url = url ? url : 'http://50.21.179.35:8082/follows/';

			$http.get(url, {
				params:this.s, timeout:this.cancellar.promise
			}).then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		getSelf: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			let url;
			if(this.s.fromMe)
				url = 'http://50.21.179.35:8082/follows/fromMe'
			if(this.s.toMe)
				url = 'http://50.21.179.35:8082/follows/toMe';

			$http.get(url, {
				params:this.s, timeout:this.cancellar.promise
			}).then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/follows/follow/'+this.a._id, this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){
			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			$http.delete('http://50.21.179.35:8082/follows/unfollow/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			'¿Esta seguro de Eliminar?',
			{
				progressBar:true,
				timeOut: 5000,
				closeButton: true,
				allowHtml:true,
				onTap: function(){
					this.remove(function(success){
						if(success){
							toastr.success('Datos Eliminados','',{progressBar:true});
							this.get();
						}
					}.bind(this));
				}.bind(this)
			});
		}
	}
	return object;
}])
//comments
app.factory('Comments',['Auth','toastr','$q','$rootScope','$http',function(Auth,toastr,$q,$rootScope,$http){
	var object = {
		s: {},
		a: {},
		b: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/comments/search', {
				params:this.s, timeout:this.cancellar.promise
			}).then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/comments',this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		put: function(cb){
			if(Auth.isLogged()){
				this.p.token = Auth.getToken();
				this.p.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}
			$http.put('http://50.21.179.35:8082/comments/id/'+this.p._id,this.p)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){
			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}

			$http.delete('http://50.21.179.35:8082/comments/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		setPut: function(item){
			this.p = angular.copy(item);
		},
		setDelete: function(item){
			this.d = angular.copy(item);

			toastr.warning('<input type="button" ng-click="remove()" class="btn btn-danger btn-raised" value="Eliminar '+this.d.name+'"/>',
			'¿Esta seguro de Eliminar?',
			{
				progressBar:true,
				timeOut: 5000,
				closeButton: true,
				allowHtml:true,
				onTap: function(){
					this.remove(function(success){
						if(success){
							toastr.success('Datos Eliminados','',{progressBar:true});
							this.get();
						}
					}.bind(this));
				}.bind(this)
			});
		}
	};
	return object;
}]);
//Likes
app.factory('Likes',['Auth','toastr','$q','$rootScope','$http','Upload',function(Auth,toastr,$q,$rootScope,$http,Upload){
	var object = {
		s: {},
		a: {},
		p: {},
		d: {},
		items: [],
		item: {},
		total: 0,
		cancellar: false,
		pageChange: function(newPage, cb){
			this.s.currentPage = newPage;
			this.s.start = this.s.limit * (this.s.currentPage-1);
			this.get(cb);
		},
		loadMore: function(){
			this.s.start = this.s.items.length;
			this.get(function(success,items){
				if(success)
					this.items.concat(items);
				if(cb)
					cb(true);
			}.bind(this), true);
		},
		get: function(cb, skip){
			if(this.cancellar)
				this.cancellar.resolve();
			this.cancellar=$q.defer();

			if(Auth.isLogged()){
				this.s.token = Auth.getToken();
				this.s.tokensecret = Auth.getTokenSecret();
			}

			$http.get('http://50.21.179.35:8082/favorites/search', {params:this.s, timeout:this.cancellar.promise})
			.then(function(response){
				this.cancellar=false;
				if (response.data.e==0){
					if(!skip)
						this.items = angular.copy(response.data.items);
					this.total = response.data.total;
					return cb ? cb(true, response.data.items) : false;
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
					return cb ? cb(false) : false;
				}
			}.bind(this),function(error){
				this.cancellar=false;
				return cb ? cb(false) : false;
			});
		},
		post: function(cb){
			if(Auth.isLogged()){
				this.a.token = Auth.getToken();
				this.a.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb) cb(false);
				return toastr.error("Debes estar logueado",'', {progressBar:true});
			}

			$http.post('http://50.21.179.35:8082/favorites',this.a)
			.then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true,angular.copy(response.data.id)) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error("Error de conexión",'', {progressBar:true});
				return cb ? cb(false) : false;
			});
		},
		remove: function(cb){
			if(Auth.isLogged()){
				this.d.token = Auth.getToken();
				this.d.tokensecret = Auth.getTokenSecret();
			}else{
				if(cb)
					cb(false);
				return toastr.error('Debe estar logueado','',{progressBar:true});
			}
			$http.delete('http://50.21.179.35:8082/favorites/id/'+this.d._id,{
				params:this.d
			}).then(function(response){
				if(response.data.e == 0){
					return cb ? cb(true) : false;
				}else{
					toastr.error(response.data.mes,'',{progressBar:true});
					return cb ? cb(false) : false;
				}
			},function(error){
				toastr.error('Error de conexión','',{progressBar:true});
				return cb ? cb(false) : false;
			});
		},
	};
	return object;
}]);
