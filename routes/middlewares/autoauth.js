var User = require('../objects/user');
	User = new User();
	
var autoauth = function(req, res, next){
	var query = req.query,
		body = req.body,
		cookies = req.cookies;
	
	if(query && (query.token && query.tokensecret)){
		return User.checkHash(query.token, query.tokensecret, function(user){
			if(user){
				req.user = user;
				return next();
			}else{
				req.user = false;
				return next();
			}
		});
	}else if(body && (body.token && body.tokensecret)){
		return User.checkHash(body.token, body.tokensecret, function(user){
			if(user){
				req.user = user;
				return next();
			}else{
				req.user = false;
				return next();
			}
		});
	}else if(cookies && (cookies.token && cookies.tokenSecret)){
		return User.checkHash(cookies.token, cookies.tokenSecret, function(user){
			if(user){
				req.user = user;
				return next();
			}else{
				res.clearCookie('token');
				res.clearCookie('tokenSecret');
				req.user = false;
				return next();
			}
		});
	}else if(cookies && (cookies.tokena && cookies.tokenSecreta)){
		return User.checkHash(cookies.tokena, cookies.tokenSecreta, function(user){
			if(user){
				req.user = user;
				return next();
			}else{
				res.clearCookie('tokena');
				res.clearCookie('tokenSecreta');
				req.user = false;
				return next();
			}
		});
	}else{
		req.user = false;
		return next();
	}
};
	
module.exports = autoauth;