var db = require('../utils/db'),
	collection = db.collection("likes"),
	ObjectID = require('mongoskin').ObjectID,
	method = Like.prototype,
	Publication = require(__dirname +'/publication'),
	Notification = require(__dirname +'/notification'),
	User = require(__dirname +'/user');

	User = new User();
	Publication = new Publication();
	Notification = new Notification();

function Like(){
	
}

method.update = function(query, order, extra, cb){
	return collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

method.count = function(filter, cb){
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al conseguir total de busqueda") : false;
		}else
			return cb ? cb(true, count) : false;
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, likes){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(likes) return cb ? cb(true, likes) : false;
		else return cb ? cb(false, "Transacciones no encontradas") : false;
	});
};

method.findOne = function(filter, cb){
	return collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar usuario") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Usuario no encontrado") : false;
	});
};

method.getMultiFull = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, items){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(items){
			var il = items.length, round = 1, i = 0, total = 0, newitems = [];
			var handleLoop = function(i){
				this.getFull(items[i], function(success, user){
					if(success)
						newitems[i] = user;
					else
						newitems[i] = items[i];
					
					if(round == il)
						return cb ? cb(true, newitems) : false;
					round++;
					return;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, items) : false;
		}
		else return cb ? cb(false, "Usuarios no encontrados") : false;
	}.bind(this));
};

method.proccess = function(item, cb){
	return User.getFull({_id: new ObjectID(item.user)}, function(success, user){
		if(success)
			item.user = user;
			
		return cb ? cb(true, item) : false;
	}.bind(this));
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		return this.findOne({_id:id}, function(success, item){
			if(success){
				return this.proccess(item, cb);
			}else
				return cb ? cb(false, "Cuenta Bancaria no encontrada") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.insert = function(body, cb){
	
	if(!body.publication || !ObjectID.isValid(body.publication))
		return cb ? cb(false, "Debes enviar a que publicación pertenece este like") : false;
	
	if(!body.user || !ObjectID.isValid(body.user))
		return cb ? cb(false, "Debes enviar el usuario que da el like") : false;
	
	var data = {
		publication: new ObjectID(body.publication),
		user: new ObjectID(body.user),
		status: 1,
		time: Date.now()
	};	
	
		
	return User.findOne({_id: data.user}, function(success, user){
		if(success){
			Publication.findOne({_id: data.publication, status:1}, function(success, publication){
				if(success){
				
					return collection.insert(data, function(err, result){
						if(err){
							console.log(err);
							return cb ? cb(false, "Error al registrar comentario") : false;
						}else{
							Notification.insert({
								subject: "Han dado um like tu publicación",
								message: user.username+" ha dado un like a tu publicación",
								type: "like",
								meta: new ObjectID(data.publication),
								user: new ObjectID(publication.user)
							});
						
							return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
						}
					});
				
				}else
					return cb ? cb(false, publication) : false;
			}.bind(this));
		}else
			return cb ? cb(false, user) : false;
	}.bind(this));
};

module.exports = Like;