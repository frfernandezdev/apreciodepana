var db = require('../utils/db'),
	collection = db.collection("message"),
	ObjectID = require('mongoskin').ObjectID,
	Chat = require(__dirname +'/chat'),
	User = require(__dirname +'/user'),
	method = Message.prototype;
	
	Chat = new Chat();
	User = new User();

function Message(){
}

method.update = function(query, order, extra, cb){
	return collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

method.count = function(filter, cb){
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else
			return cb ? cb(true, users) : false;
	});
};

method.findOne = function(filter, cb){
	return collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else
			return cb ? cb(false, "Message no encontrada") : false;
	});
};

method.getMultiFull = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, users){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(users){
			var il = users.length, round = 1, i = 0, total = 0, newUsers = [];
			var handleLoop = function(i){
				this.getFull(users[i], function(success, user){
					if(success)
						newUsers[i] = user;
					else
						newUsers[i] = users[i];
					
					if(round == il)
						return cb ? cb(true, newUsers) : false;
					round++;
					return;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, users) : false;
		}
		else return cb ? cb(false, "Mensajes no encontrados") : false;
	}.bind(this));
};

method.proccess = function(message, cb){
	return User.getFull(new ObjectID(message.user), function(success, user){
		if(success)
			message.user = user;
		return cb ? cb(true, user) : false;
	});
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		this.findOne({_id:id}, function(success, message){
			if(success){
				return this.proccess(message, cb);
			}else
				return cb ? cb(false, "Mensaje no encontrado") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};


method.insert = function(data, cb){
	if(ObjectID.isValid(data.chat) &&
		ObjectID.isValid(data.user) &&
		data.message &&
		data.type){
			
		data.chat = new ObjectID(data.chat);	
		data.user = new ObjectID(data.user);	
		data.time = Date.now();	
			
			
		return Chat.findOne({_id:data.chat}, function(success, chat){
			if(success){
				return collection.insert(data, function(err, result){
					if(err){
						console.log(err);
						return cb ? cb(false, "Error al insertar") : false;
					}else{
						data._id = new ObjectID(result.insertedIds[0]);
						return this.getFull(data, function(success, data){
							return cb ? cb(true, data) : false;
						});
					}
				}.bind(this));
			}else
				return cb ? cb(false, "Chat no encontrado") : false;
		}.bind(this));
	}else
		return cb ? cb(false, "Debes enviar todos los campos") : false;
};

module.exports = Message;