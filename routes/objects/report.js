var db = require('../utils/db'),
	collection = db.collection("reports"),
	ObjectID = require('mongoskin').ObjectID,
	method = Report.prototype,
	Transaction = require(__dirname +'/transaction'),
	User = require(__dirname +'/user');

	User = new User();
	Transaction = new Transaction();

function Report(){

}

method.update = function(query, order, extra, cb){
	return collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.doUpdate = function(user, id, body, old, cb){
	var data = {};

	if(!ObjectID.isValid(id))
		return cb ? cb(false, "ID Invalido") : false;
	id = new ObjectID(id);

	if(body.status && old.status == 1)
		data.status = parseInt(body.status);

	if(Object.keys(data).length > 0){
		return this.update({_id:id}, {"$set":data}, {}, function(success, mes){
			if(success){
				return cb ? cb(true, data.bitacora[0]) : false;
			}else{
				return cb ? cb(false, mes) : false;
			}
		});
	}else
		return cb ? cb(false, "Debes enviar al menos un campo para actualizar") : false;
};
method.preparefind = function(body){
	var find = {};

	if(body.openby && ObjectID.isValid(body.openby))
		find.openby = new ObjectID(body.openby)

	if(body.openfor && ObjectID.isValid(body.openfor))
		find.openfor = new ObjectID(body.openfor)

	if(body.transaction && ObjectID.isValid(body.transaction))
		find.transaction = new ObjectID(body.transaction)

	if(body.mintime){
		if(!find.time) find.time = {}
		find.time["$gte"] = this.dateToTime(body.mintime);
	}

	if(body.maxtime){
		if(!find.time) find.time = {}
		find.time["$lte"] = this.dateToTime(body.maxtime);
	}

	if(body.status){
		find.status = parseInt(body.status);
	}
	return find;
};

method.count = function(filter, cb){
	filter = this.preparefind(filter);
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al conseguir total de busqueda") : false;
		}else
			return cb ? cb(true, count) : false;
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}

	filter = this.preparefind(filter);
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);

	return promise.toArray(function(err, reports){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(reports) return cb ? cb(true, reports) : false;
		else return cb ? cb(false, "Transacciones no encontradas") : false;
	});
};

method.findOne = function(filter, cb){
	return collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar usuario") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Usuario no encontrado") : false;
	});
};

method.getMultiFull = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}

	filter = this.preparefind(filter);
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);

	return promise.toArray(function(err, items){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(items){
			var il = items.length, round = 1, i = 0, total = 0, newitems = [];
			var handleLoop = function(i){
				this.getFull(items[i], function(success, user){
					if(success)
						newitems[i] = user;
					else
						newitems[i] = items[i];

					if(round == il)
						return cb ? cb(true, newitems) : false;
					round++;
					return;
				}.bind(this));
				return;
			}.bind(this);

			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, items) : false;
		}
		else return cb ? cb(false, "Usuarios no encontrados") : false;
	}.bind(this));
};

method.proccess = function(item, cb){
	return User.getFull({_id: new ObjectID(item.openby)}, function(success, user){
		if(success)
			item.openby = user;

		return User.getFull({_id: new ObjectID(item.openfor)}, function(success, user){
			if(success)
				item.openfor = user;

			return Transaction.getFull({_id: new ObjectID(item.transaction)}, function(success, transaction){
				if(success)
					item.transaction = transaction;

				return cb ? cb(true, user) : false;
			});
		});
	});
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		return this.findOne({_id:id}, function(success, item){
			if(success){
				return this.proccess(item, cb);
			}else
				return cb ? cb(false, "Reporte no encontrado") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.getFullTransaction = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		return this.findOne({transaction: id}, function(success, item){
			if(success){
				return cb ? cb(true, item): false;
			}else
				return cb ? cb(false, "Reporte no encontrado") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.insert = function(body, cb){

	if(!body.transaction || !ObjectID.isValid(body.transaction))
		return cb ? cb(false, "Debes enviar a que transacción pertenece este reporte") : false;

	if(!body.by || !ObjectID.isValid(body.by))
		return cb ? cb(false, "Debes enviar el comprador esta transacción") : false;

	if(!body.subject)
		return cb ? cb(false, "Debes enviar el asunto asociado a este reporte") : false;

	if(!body.comment)
		return cb ? cb(false, "Debes enviar un comentario asociado a este reporte") : false;

	var data = {
		transaction: new ObjectID(body.transaction),
		subject: body.subject,
		comment: body.comment,
		status: 1,
		openby: new ObjectID(body.by),
		time: Date.now()
	};


	return User.findOne({_id: data.openby}, function(success, user){
		if(success){
			Transaction.findOne({_id: data.transaction}, function(success, transaction){
				if(success){

					var buyerRatingSeller = true;

					if(new ObjectID(transaction.buyer).equals(data.openby)){
						data.openbytype = 1;
						data.openfor = new ObjectID(transaction.seller);
					}else if(new ObjectID(transaction.seller).equals(data.openby)){
						data.openbytype = 2;
						data.openfor = new ObjectID(transaction.buyer);
					}else
						return cb ? cb(false, "No puedes reportar esta transacción") : false;

					return collection.insert(data, function(err, result){
						if(err){
							console.log(err);
							return cb ? cb(false, "Error al agregar reporte") : false;
						}else{
							return cb ? cb(true) : false;
						}
					});
				}else
					return cb ? cb(false, publication) : false;
			});
		}else
			return cb ? cb(false, user) : false;
	});
};

module.exports = Report;
