var db = require('../utils/db'),
	collection = db.collection("view"),
	ObjectID = require('mongoskin').ObjectID,
	method = View.prototype,
	fileUtils = require('../utils/fileUtils');

function View(){
}

method.dateToTime = function(date){
	date=date.split("-");
	return new Date(date[2]+"-"+date[1]+"-"+date[0]).getTime();
};

method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

method.count = function(filter, cb){
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else
			return cb ? cb(true, users) : false;
	});
};

method.findOne = function(filter, cb){
	return collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else
			return cb ? cb(false, "Categoria no encontrada") : false;
	});
};

method.insert = function(data, cb){
	if(data.age !== undefined &&
		data.gender !== undefined &&
		ObjectID.isValid(data.publication)){
		
		data.time = Date.now();
		
		return collection.insert(data, function(err, result){
			if(err){
				return cb ? cb(false, "Error al insertar") : false;
			}else{
				return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
			}
		});
	}else
		return cb ? cb(false, "Debes enviar todos los campos") : false;
};

module.exports = View;