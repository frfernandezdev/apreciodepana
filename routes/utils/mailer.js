var mailer = require('nodemailer'),
	transporter = mailer.createTransport({
		host: 'mail.apreciodepana.com',
		port: 465,
		secure: true, // use SSL
		tls: {rejectUnauthorized: false},
		auth: {
			user: 'noreplay@apreciodepana.com',
			pass: 'agL0zdnQ2kuB'
		}
	});


/*transporter.verify(function(error, success) {
   if (error) {
        console.log(error);
   } else {
        console.log('Server is ready to take our messages');
   }
});*/
var footer = 'PIE DE PÁGINA';

var wrapBody = (body) => {
	return 'CABECERA<br><br>'+body+'<br><br>'+footer+'';
};

var forgotMail = (name,token,tokenSecret) => {
	return {
		from: '"OKApp" <no-responder@okapp.website>',
		subject: 'Recupera tu contraseña', // Subject line
		text: 'Hola '+name+'.\n\nSe ha solicitado restaurar la contraseña de tu cuenta. Para  realizar el cambio da click en el siguiente enlace:\n\n http://okapp.website/forgotpassword/'+token+'/'+tokenSecret, // plaintext body
		html: wrapBody('<h3>Hola '+name+'.</h3><br><br>Se ha solicitado restaurar la contraseña de tu cuenta. Para  realizar el cambio da click en el siguiente enlace.<br><br><a href="http://okapp.website/forgotpassword/'+token+'/'+tokenSecret+'">Restaurar Contraseña</a>') // html body
	};
};

var confirmMail = (name, mail, mailToken) => {
	return {
		from: '"OKApp" <no-responder@okapp.website>',
		subject: 'Confirma tu E-mail', // Subject line
		text: 'Hola '+name+'.\n\nPara comenzar a usar OKApp como usuario es necesario que confirmes tu cuenta.\n\n Por favor haz click en el seguiente enlance:\n\n http://okapp.website/register/mail/'+mail+'/'+mailToken+'\n\n', // plaintext body
		html: wrapBody('<h3>Hola '+name+'.</h3><br><br>Para comenzar a usar OKApp como usuario es necesario que confirmes tu cuenta.<br><br> Por favor haz click en el seguiente enlance: <a href="http://okapp.website/register/mail/'+mail+'/'+mailToken+'">http://okapp.website/register/mail/'+mail+'/'+mailToken+'</a><br><br>') // html body
	};
};


var youWon = (name, raffleName, link) => {
	return {
		from: '"OKApp" <no-responder@okapp.website>',
		subject: 'Felicidades al ganar '+raffleName, // Subject line
		text: 'Hola '+name+'.\n\n:\n\n Felicidades por ganar la rifa '+raffleName+', pronto nos pondremos en contacto contigo\n\n', // plaintext body
		html: wrapBody('<h3>Hola '+name+'.</h3><br><br>Felicidades por ganar la rifa <b>'+raffleName+'</b>, pronto nos pondremos en contacto contigo') // html body
	};
};

var Contact = (body) => ({
	from: `APrecioDePana <noreplay@apreciodepana.com>`,
	replyTo: `"${body.name}" <${body.mail}>`,
	to: `${body.mail}`,
	encoding : 'utf-8',
	subject: 	"Contacto",
	html: `
		<div>
			<div>
					<img src="http://50.21.179.35:8083/logo-header.png" width="100" height="100"/>
			</div>
		<div>
	`
	// html: `
	// <div>
	// 	<div>
	// 		<a href="http://50.21.179.35:8080/">
	// 			<img src="http://50.21.179.35:8083/logo-header.png" width="100" height="100"/>
	// 		</a>
	// 	</div>
	// 	<hr style="border-bottom: 1px solid #ccc;padding: 10px;">
	// 	<div>
	// 		<h3>
	// 			<b>Nuevo mail de Contacto</b>
	// 		</h3>
	// 		<span>Nuevo mail de:</span>
	// 		<span>${body.mail}</span>
	// 		<br/>
	// 		<span>Nombre:</span>
	// 		<span>${body.name}</span>
	// 		<br/>
	// 		<span>Email:</span>
	// 		<span>${body.mail}</span>
	// 		<span>Asunto: Contacto:</span>
	// 		<span>Mensaje:</span>
	// 		<p>${body.body}</p>
	// 		<hr style="border-top: 3px solid #fc7200;padding: 10px;">
	// 	</div>
	// 	<div>
	// 		<span style="font-size: 12px;color: #ccc;">A Precio De Pana, C.A. RIF J-405824794 <b>Venezuela</b></span>
	// 		<br/>
	// 		<a href="https://www.facebook.com/PanasEnLinea" style="text-decoration: none;">
	// 			<img src="http://50.21.179.35:8083/facebook.png" alt="" width="auto" height="50" style="margin-top: 10px;">
	// 		</a>
	// 		<a href="https://twitter.com/panasenlinea" style="text-decoration: none;">
	// 			<img src="http://50.21.179.35:8083/twitter.png" alt="" width="auto" height="50" style="margin-top: 10px;">
	// 		</a>
	// 		<a href="https://www.youtube.com/channel/UCsQhTuFD9UGw3mleWd_n1jg" style="text-decoration: none;">
	// 			<img src="http://50.21.179.35:8083/instagram.png" alt="" width="auto" height="50" style="margin-top: 10px;">
	// 		</a>
	// 		<a href="https://www.instagram.com/apreciodepana/" style="text-decoration: none;">
	// 			<img src="http://50.21.179.35:8083/youtube.png" alt="" width="auto" height="50" style="margin-top: 10px;">
	// 		</a>
	// 	</div>
	// </div>`
})

var ConfirmSales = () => {
	from: `APrecioDePana <noreplay@apreciodepana.com>`
};

var sendMail = (mailOptions) => {
	mailOptions.encoding = 'utf-8';
	transporter.sendMail(mailOptions, (error, info) => {
		if(error)
			return console.log(error);
		console.log('Message sent: ' + info.response);
	});
};

module.exports = {
	wrapBody: wrapBody,
	sendMail: sendMail,
	confirmMail: confirmMail,
	forgotMail: forgotMail,
	Contact: Contact,
	ConfirmSales: ConfirmSales,
};
